<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\CompanyService;


class AdminController extends Controller
{
    public function __construct(
        CompanyService $companyService
    ) {
        $this->company = $companyService;
    }

    public function getCompanyUsers(Request $request)
    {
        $companies = $this->company->all();

        foreach ($companies as $company) {
            $company->users = $company->getRelations();
        }
    }
}
