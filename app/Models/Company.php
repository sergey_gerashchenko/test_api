<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Company extends Model
{
    use HasFactory;

    public $incrementing = false;

    public static $unguarded = true;

    protected $fillable = [
        'id',
        'name',
        'started_at',
    ];

    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
