<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;
use App\Models\Company;

class ParseJSON extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'json:parse {path}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse JSON file and add parsed information to DB';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $fileContent = file_get_contents($this->argument('path'));

        $data = json_decode($fileContent, true);

        foreach ($data as $users) {
            foreach ($users as $user) {

                $userSave = new User;

                $userSave->id = $user['id'];
                $userSave->name = $user['name'];
                $userSave->age = $user['age'];

                $userSave->save();

                if (!empty($user['companies'])) {
                    foreach ($user['companies'] as $company) {
                        $companySave = Company::firstOrCreate([
                            'id'   => $company['id'],
                            'name' => $company['name'],
                            'started_at' => $company['started_at']
                        ]);

                        $companySave->users()->attach($userSave);
                    }
                }
            }
        }
    }
}
